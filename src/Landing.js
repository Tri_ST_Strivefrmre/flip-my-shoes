import React from 'react';

import ImgContainer from './components/ImgContainer';
import Image from './components/Image';
import { NavLink } from "react-router-dom";

const Landing = (props) =>(
    <div>
    <div>
      <ImgContainer 
        heading= 'New Balance Fuel Cell Rebel 😍'
        subheading = 'Use your fingers/mouse to rotate 360º'>
        <Image/>
      </ImgContainer>
    </div>
    <div style={{'marginTop':'5%'}}>
      <h6 className="text-muted"> Actual Representation</h6>
      <br/>
      <NavLink to='/about'>Learn More</NavLink>
    </div>
    </div>
)

export default Landing;