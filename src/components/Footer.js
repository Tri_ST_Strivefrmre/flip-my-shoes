import React from 'react';

import { Navbar } from 'react-bootstrap';


const Footer = (props) =>(
<Navbar className="justify-content-center" bg="light" expand="lg" style={{'border-top': 'none'}}>
<i className="far fa-copyright"></i>
<a href="http://www.shaarangtanpure.com" target="_blank" rel="noopener noreferrer" style={{'marginLeft':'10px'}}>
    Shaarang Tanpure
</a>
<a href="/" className="text-muted" style={{'marginLeft':'5px', 'pointerEvents':'none'}}>(During 2020 Isolation period) </a>
</Navbar>
)

export default Footer;
