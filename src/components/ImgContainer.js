import React from "react";

import { Container, Row, Col } from "react-bootstrap";

const ImgContainer = props => (
  <Container>
    <Row>
      <Col style={{'margin-top':'5%'}}>
        <h1 style={{ 'font-family': 'Noto Sans, sans-serif;'}}>
          {props.heading}
        </h1>
      </Col>
    </Row>
    <Row>
      <Col style={{'margin-top':'2%'}}>
        <h4 style={{ 'font-family': 'Noto Sans, sans-serif;'}} className="text-muted">
          {props.subheading}
        </h4>
      </Col>
    </Row>
    <Row style={{'margin-top': '20px'}}>
      <Col> 
      {props.children}
      </Col>
    </Row>
  </Container>
);

export default ImgContainer;
