import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";


const NavigationBar = props => (
  <div  style={{'borderBottom':'1px Black'}}>
  <Navbar bg="white" expand="lg">
    <Navbar.Brand>
      <NavLink className="Nav-link" to="/"
      exact
      activeStyle={{
          color:'#464e91',
          textDecoration: 'none'
      }}
      > An ode to my new running shoes </NavLink>
    </Navbar.Brand>
    <Navbar.Toggle />
    <Navbar.Collapse
      className="justify-content-end"
      style={{ "padding-right": "40px" }}>
      <Nav.Item>
        <NavLink className="Nav-link" to="/about">/about</NavLink>
      </Nav.Item>
    </Navbar.Collapse>
  </Navbar>
  
  </div>
);

export default NavigationBar;
