import React from "react";
import Rotation from "react-rotation";
import { Image } from "react-bootstrap";
import LazyLoad from 'react-lazyload';


const Img = (props) => (
  <LazyLoad height={100} offset={100}>
  <Rotation pauseOnHover={true} reverse={true}>
    <Image alt="1" src="https://i.imgur.com/I8tlAw0.png"/>
    
    <Image alt="2" src="https://i.imgur.com/PG6Iwt4.png"/>
    <Image alt="3" src="https://i.imgur.com/PXFxmQU.png"/>
    <Image alt="4" src="https://i.imgur.com/cJXfbgA.png"/>
    <Image alt="5" src="https://i.imgur.com/mTgMgF1.png"/>
    <Image alt="6" src="https://i.imgur.com/YIu8808.png"/>
    
  </Rotation>
  </LazyLoad>
);

export default Img;
