import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import NavigationBar from "./components/Nav/Nav";
import Footer from "./components/Footer";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import About from "./pages/About";
import Landing from "./Landing";

function App() {
  return (
    <BrowserRouter>
      <div>
        <div className="App">
          <NavigationBar />
          <Switch>
            <Route exact path="/" component={Landing} />
            <Route path="/about" component={About} />
            {/*<Route path="/:id" component={FullPost}/>*/}
          </Switch>
        </div>
        <div className="Footer">
          <Footer />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
