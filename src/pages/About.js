import React from "react";
import "./About.css";
import { Container, Row, Col, Table } from "react-bootstrap";
import { HashLink } from "react-router-hash-link";

const About = props => (
  <Container fluid style={{ marginRight: "5px" }}>
    <Row className="Sub-nav">
      <Col md={8}>
        <HashLink className="Sub-link" smooth to="/about#Why">
          Why?
        </HashLink>
        <HashLink className="Sub-link" style={{'marginLeft':'5%'}} smooth to="/about#Shoes">
          Shoes
        </HashLink>
        <HashLink className="Sub-link" style={{'marginLeft':'5%'}} smooth to="/about#Credits">
          Credits
        </HashLink>
      </Col>
    </Row>
    <Row className="Spacing">
      <Col md={3}>
        <h1 id="Why" className="Text-color">
          <HashLink className="Sub-link" smooth to="/about#Why">
            #
          </HashLink>
          Why?
        </h1>
      </Col>
    </Row>
    <Row className="Text-Space Spacing">
      <Col md={{ span: 6, offset: 1 }} style={{'marginRight': '10px'}}>
        <h6 className="Text-color Desc">
          Why not?
          <br />
          <br />
          Who would have thought that a virtual drink session over <strong>Zoom</strong> with friends would end up in a code challenge [all of this while drinking wine].
          <br/>
          I thought this would be perfect opportunity for documenting my experience with my new found love. I clicked a few pictures from different angles and with some boiler-plating, Vola! We have a website.
          <br/>
          Some pointers for why - 
          <br/>
          <ol>
            <li>
              For a runner, it's very hard to find a shoe that just fits you
              right and as soon as you step out in them, you fall in love.
            </li>

            <li>
              Also, due to the lockdown's and cancelled long group-rides and swim sessions, it's
              very hard to keep myself motivated and productive before I fall in
              the black hole of Netflix, binge watching 'Schitt's Creek' (which
              by the way is hilarious.
              <span role="img" aria-label="heart">
                ❤️
              </span>{" "}
              David Rose).{" "}
            </li>

            <li>
              Also, I had recently decided to do a{" "}
              <a
                href="http://7days7websites.glitch.me/"
                target="_blank"
                rel="noopener noreferrer"
              >
                7day7websites challenge{" "}
              </a>
              to test out my skills in different FE frameworks like Angular,
              React and Vue to build something cool. This is a perfect time when I can do that while sipping on Coffee and
              eating
              <a
                href="https://www.arnotts.com/products/tim-tam"
                target="_blank"
                rel="noopener noreferrer"
              >
                TimTam's.
              </a>
            </li>
          </ol>
        </h6>
      </Col>
    </Row>
    <Row className="Spacing">
      <Col md={3}>
        <h1 id="Shoes" className="Text-color">
          <HashLink className="Sub-link" smooth to="/about#TheShoes">
            #
          </HashLink>
          Shoes
        </h1>
      </Col>
    </Row>
    <Row className="Text-Space Spacing">
      <Col md={{ span: 6, offset: 1 }} style={{'marginRight': '10px'}}>
        <h6 className="Text-color Desc" >
          The shoes are just PURRRRRFFFFEECCCTTT!
          <br />
          Here are some of its details
          <br /> <br />
          <Table striped bordered hover>
            <tbody>
              <tr>
                <td>Type</td>
                <td>Neutral</td>
              </tr>
              <tr>
                <td>Weight</td>
                <td>
                  7.2oz == 212g{" "}
                  <span role="img" aria-label="astonished">
                    😲
                  </span>
                </td>
              </tr>
              <tr>
                <td>Heel-to-toe offset</td>
                <td>5-7mm</td>
              </tr>
              <tr>
                <td>Stack height</td>
                <td>24mm (heel), 18mm (forefoot)</td>
              </tr>
            </tbody>
          </Table>
          <br />
          The shoes do not offer as much bounce as my Nike ZoomFly 3 but since
          they are light-weight, they propel you forward with minimal
          resistance.
          <br />
          The shoes have a bigger mid-sole area (assuming NB built in for better
          impact) and are tappered towards the heal. Due to this, your body
          subconsciously tries to land on the mid-foot.
          <br />
          I have struggled to do that in the past mainly because I used to
          heel-strike and shoes like Hoka's supported me well but this habit
          made me susceptible to more injuries when I switched from Hokas to
          Nikes.
          <br />
          I initially bought them for doing long-runs but seems like they are
          going to be my all-round shoe. Nevertheless, I think I'm going to
          stick to these shoes and the brand for a long time.
          <br/>
          You can find a much better review of the shoes on<strong> Seth James DeMoor's </strong><a href="https://www.youtube.com/watch?v=Id8jWaW4SWI"> youtube  channel</a>. 
          <br />
          <br />
          Cheers New-Balance{" "}
          <span role="img" aria-label="thumbs-up">
            👍{" "}
          </span>
        </h6>
      </Col>
    </Row>

    <Row className="Spacing">
      <Col md={3}>
        <h1 id="Credits" className="Text-color">
          <HashLink className="Sub-link" smooth to="/about#Credits">
            #
          </HashLink>
          Credits
        </h1>
      </Col>
    </Row>
    <Row className="Text-Space Spacing">
      <Col md={{ span: 6, offset: 1 }} style={{'marginRight': '10px'}}>
        <h6 className="Text-color Desc">
          Finally, credits to -
          <br />
          <br />
          <ol>
            <li>New Balance - For making such awesome shoes!</li>

            <li> My stupid/idiot drunk friends</li>

            <li>
              Andre Polischuk - creator of
              <a
                href="https://github.com/andrepolischuk/react-rotation"
                target="_blank"
                rel="noopener noreferrer"
              >
                {" "}
                react-rotation{" "}
              </a>
              component package. I've been wanting to make use of this package since quite some time.
            </li>
            <li>
              Florian Pop - for coming up with the{" "}
              <a
                href="http://7days7websites.glitch.me/"
                target="_blank"
                rel="noopener noreferrer"
              >
                7day7websites challenge.
              </a>
            </li>
            <li>
                Lastly, my bank account for being able to afford them, lol. (RP: AUD$200, OnSalePrice: AUD$125)
            </li>
          </ol>
        </h6>
      </Col>
    </Row>
    <Row className="Spacing">
        <Col>
        <h5 className="Text-color"> # THE END #</h5>
        </Col>
    </Row>
  </Container>
);

export default About;
